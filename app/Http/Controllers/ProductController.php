<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use Exception;
use Illuminate\Http\Request as HttpRequest;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Product::all();
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function findByID($id)
    {
        $data = Product::find($id);
        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function save(StoreProductRequest $request)
    {
        try {
            $request->validate([
                "nama_produk" => 'required',
                "kategori" => 'required',
                "stok" => 'required',
                "harga" => 'required',
            ]);

            $data = new Product();
            $data->nama_produk = $request->nama_produk;
            $data->kategori = $request->kategori;
            $data->stok = $request->stok;
            $data->harga = $request->harga;
            $data->save();
            return response()->json($data);
        } catch (Exception $e) {
            return response()->json(['message'=>'Failed'.$e->getMessage()],400);
        };
    }

    /**
     * Display the specified resource.
     */
    public function updateProduct($id,HttpRequest $product)
    {
        $data = Product::find($id);
            $data->nama_produk = $product->nama_produk ?? $data->nama_produk;
            $data->kategori = $product->kategori ?? $data->kategori;
            $data->stok = $product->stok ?? $data->stok;
            $data->harga = $product->harga ?? $data->harga;
            $data->save();
            return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function deleteProduct($id)
    {
        $data = Product::find($id);
        $tampil = json_decode(json_encode($data));

        $data->delete();


        return response()->json(['delete'=>true, 'product'=>$tampil]);
    }

}
