<?php

use App\Http\Controllers\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("v1/product/",[ProductController::class,'index']);
Route::get("v1/product/{id}",[ProductController::class,'findByID']);
Route::post("v1/product/",[ProductController::class,'save']);
Route::put("v1/product/{id}",[ProductController::class,'updateProduct']);
Route::delete("v1/product/{id}",[ProductController::class,'deleteProduct']);